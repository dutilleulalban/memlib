# MemLib

# Introduction

A C# library for editing and managing the memory of Windows applications.
It offers a set of modules to have a simplified access to a certain number of tasks: call functions in a process, search for patterns, ... 

# Features

 *  Retrieve various information about the process
 *  Read/Write primitive and complex data types
 * Shellcode injection and execution
 * Calling a function by specifying the convention used
 * Search Patterns

# Install

## Requirements

1. .NET Core 5.0

# Copyright

**License** : MIT

# Usage

Coming soon, in the meantime you can already go and look in the source code.
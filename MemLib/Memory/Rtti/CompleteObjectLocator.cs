﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;

namespace MemLib.Memory.Rtti
{
    public sealed class CompleteObjectLocator : RemotePointer
    {
        public int Signature => TryRead<int>(out var val) ? val : 0;
        public int Offset => TryRead<int>(0x04, out var val) ? val : 0;
        public int CdOffset => TryRead<int>(0x08, out var val) ? val : 0;

        private TypeDescriptor m_TypeDescriptor;
        public TypeDescriptor TypeDescriptor
        {
            get
            {
                if (m_TypeDescriptor != null)
                    return m_TypeDescriptor;
                if (Process.Is64Bit)
                {
                    var baseAddress = Process.MainModule.BaseAddress;
                    if (!TryRead<int>(0x0C, out var tdOffset))
                        return null;
                    var tdAddress = baseAddress + tdOffset;
                    m_TypeDescriptor = new TypeDescriptor(Process, tdAddress);
                }
                else
                {
                    if (!TryRead<IntPtr>(0x0C, out var tdAddress))
                        return null;
                    m_TypeDescriptor = new TypeDescriptor(Process, tdAddress);
                }
                return m_TypeDescriptor;
            }
        }

        private ClassHierarchyDescriptor m_ClassHierarchyDescriptor;
        public ClassHierarchyDescriptor ClassHierarchyDescriptor
        {
            get
            {
                if (m_ClassHierarchyDescriptor != null)
                    return m_ClassHierarchyDescriptor;
                if (Process.Is64Bit)
                {
                    var baseAddress = Process.MainModule.BaseAddress;
                    if (!TryRead<int>(0x10, out var chdOffset))
                        return null;
                    var chdAddress = baseAddress + chdOffset;
                    m_ClassHierarchyDescriptor = new ClassHierarchyDescriptor(Process, chdAddress);
                }
                else
                {
                    if (!TryRead<IntPtr>(0x10, out var chdAddress))
                        return null;
                    m_ClassHierarchyDescriptor = new ClassHierarchyDescriptor(Process, chdAddress);
                }
                return m_ClassHierarchyDescriptor;
            }
        }

        internal CompleteObjectLocator(RemoteProcess process, IntPtr address) : base(process, address) { }
    }
}
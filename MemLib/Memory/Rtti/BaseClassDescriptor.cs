﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;

namespace MemLib.Memory.Rtti
{
    public sealed class BaseClassDescriptor : RemotePointer
    {
        public TypeDescriptor TypeDescriptor { get; }
        public int NumContainedBases => Read<int>(0x04);
        public int PDisp => Read<int>(0x08);
        public int MDisp => Read<int>(0x0C);
        public int VDisp => Read<int>(0x10);
        public int Attributes => Read<int>(0x14);

        internal BaseClassDescriptor(RemoteProcess process, IntPtr address) : base(process, address)
        {
            if (process.Is64Bit)
            {
                if (TryRead<int>(out var tdOffset))
                    TypeDescriptor = new TypeDescriptor(process, process.MainModule.BaseAddress + tdOffset);
            }
            else
            {
                if (TryRead<IntPtr>(out var tdAddress))
                    TypeDescriptor = new TypeDescriptor(process, tdAddress);
            }
        }
    }
}
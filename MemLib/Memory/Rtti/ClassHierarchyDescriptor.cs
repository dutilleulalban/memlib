﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace MemLib.Memory.Rtti
{
    public sealed class ClassHierarchyDescriptor : RemotePointer
    {
        public int Signature => TryRead<int>(out var val) ? val : 0;
        public int Attributes => TryRead<int>(0x04, out var val) ? val : 0;
        public int NumBaseClasses => TryRead<int>(0x08, out var val) ? val : 0;

        private List<BaseClassDescriptor> m_BaseClassArray;
        public List<BaseClassDescriptor> BaseClassArray
        {
            get
            {
                if (m_BaseClassArray != null)
                    return m_BaseClassArray;
                var count = NumBaseClasses;
                if (count <= 0)
                    return m_BaseClassArray = new List<BaseClassDescriptor>();
                if (Process.Is64Bit)
                {
                    if (!TryRead<int>(0x0C, out var arrayOffset))
                        return m_BaseClassArray = new List<BaseClassDescriptor>();
                    var baseAddress = Process.MainModule.BaseAddress;
                    var arrayAddress = baseAddress + arrayOffset;
                    m_BaseClassArray = new List<BaseClassDescriptor>(count);
                    for (var i = 0; i < count; i++)
                    {
                        if (!Process.TryRead<int>(arrayAddress + i * 4, out var offset))
                            break;
                        BaseClassArray.Add(new BaseClassDescriptor(Process, baseAddress + offset));
                    }
                }
                else
                {
                    var arrayAddress = Read<IntPtr>(0x0C);
                    m_BaseClassArray = new List<BaseClassDescriptor>(count);
                    for (var i = 0; i < count; i++)
                    {
                        if (!Process.TryRead<IntPtr>(arrayAddress + i * 4, out var addr))
                            break;
                        BaseClassArray.Add(new BaseClassDescriptor(Process, addr));
                    }
                }
                return m_BaseClassArray;
            }
        }

        internal ClassHierarchyDescriptor(RemoteProcess process, IntPtr address) : base(process, address) { }
    }
}
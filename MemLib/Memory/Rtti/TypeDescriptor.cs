﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Text;
using MemLib.Native;

namespace MemLib.Memory.Rtti
{
    public sealed class TypeDescriptor : RemotePointer
    {
        public IntPtr VTablePointer => TryRead<IntPtr>(out var val) ? val : IntPtr.Zero;

        private string m_Name;
        public string Name
        {
            get
            {
                if (!string.IsNullOrEmpty(m_Name))
                    return m_Name;
                if (!TryReadString(Process.Is64Bit ? 0x10 : 0x08, out var name, Encoding.UTF8, 256))
                    return m_Name = string.Empty;
                return m_Name = name;
            }
        }

        private string m_DemangledName;
        public string DemangledName
        {
            get
            {
                if (!string.IsNullOrEmpty(m_DemangledName))
                    return m_DemangledName;
                var sb = new StringBuilder(256);
                NativeMethods.UnDecorateSymbolName($"{Name.Replace(".?AV", "?")}", sb, 256, UnDecorateFlags.NameOnly);
                return m_DemangledName = sb.ToString().Trim();
            }
        }

        internal TypeDescriptor(RemoteProcess process, IntPtr address) : base(process, address) { }
    }
}
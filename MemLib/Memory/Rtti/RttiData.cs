﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace MemLib.Memory.Rtti
{
    public sealed class RttiData : RemotePointer
    {
        public CompleteObjectLocator CompleteObjectLocator { get; }
        public TypeDescriptor TypeDescriptor => CompleteObjectLocator?.TypeDescriptor;
        public ClassHierarchyDescriptor ClassHierarchyDescriptor => CompleteObjectLocator?.ClassHierarchyDescriptor;

        public string ClassName => TypeDescriptor?.DemangledName ?? string.Empty;
        public IEnumerable<string> BaseClassNames => ClassHierarchyDescriptor?.BaseClassArray.Select(c => c.TypeDescriptor.DemangledName);

        #region Overrides of RemotePointer

        public override bool IsValid
        {
            get
            {
                if (CompleteObjectLocator == null || !CompleteObjectLocator.IsValid || !base.IsValid)
                    return false;
                if (TypeDescriptor == null || !TypeDescriptor.IsValid ||
                    ClassHierarchyDescriptor == null || !ClassHierarchyDescriptor.IsValid)
                    return false;
                return TypeDescriptor.Name.Length > 0 && TypeDescriptor.Name.StartsWith(".?AV");
            }
        }

        public override string ToString()
        {
            var names = BaseClassNames?.ToList();
            var nameStr = names == null ? ClassName : string.Join(" : ", names);
            return string.IsNullOrEmpty(nameStr) ? $"{base.ToString()} No RTTI Info" : nameStr;
        }

        #endregion

        internal RttiData(RemoteProcess process, IntPtr address) : base(process, address)
        {
            if (!process.TryRead<IntPtr>(address, out var vtable))
                return;
            if (!process.TryRead<IntPtr>(vtable - IntPtr.Size, out var colAddress) || colAddress.ToInt64() <= 4096)
                return;
            CompleteObjectLocator = new CompleteObjectLocator(process, colAddress);
        }
    }
}
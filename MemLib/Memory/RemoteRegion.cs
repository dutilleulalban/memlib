﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using MemLib.Native;

namespace MemLib.Memory
{
    public class RemoteRegion : RemotePointer, IEquatable<RemoteRegion>
    {
        public IntPtr AllocationBase => MemoryBasicInformation.AllocationBase;
        public MemoryProtectionFlags AllocationProtect => MemoryBasicInformation.AllocationProtect;
        public MemoryProtectionFlags Protection => MemoryBasicInformation.Protect;
        public MemoryStateFlags State => MemoryBasicInformation.State;
        public MemoryTypeFlags Type => MemoryBasicInformation.Type;
        public virtual IntPtr Size => MemoryBasicInformation.RegionSize;
        public override bool IsValid => base.IsValid && State != MemoryStateFlags.Free;

        private MemoryBasicInformation? m_MemoryBasicInformation;
        public MemoryBasicInformation MemoryBasicInformation
        {
            get
            {
                if (m_MemoryBasicInformation.HasValue)
                    return m_MemoryBasicInformation.Value;
                m_MemoryBasicInformation = MemoryManager.Query(Process.Handle, BaseAddress);
                return m_MemoryBasicInformation.Value;
            }
        }

        public RemoteRegion(RemoteProcess process, IntPtr address) : base(process, address) { }

        internal RemoteRegion(RemoteProcess process, IntPtr address, MemoryBasicInformation mbi) : base(process, address)
        {
            m_MemoryBasicInformation = mbi;
        }

        public void Release()
        {
            MemoryManager.Free(Process.Handle, BaseAddress);
            BaseAddress = IntPtr.Zero;
        }

        public MemoryProtection ChangeProtection(MemoryProtectionFlags protection = MemoryProtectionFlags.ExecuteReadWrite, bool mustBeDisposed = true)
        {
            return new MemoryProtection(Process, BaseAddress, Size.ToInt64(), protection, mustBeDisposed);
        }

        #region Overrides of RemotePointer

        public override string ToString()
        {
            return !IsValid ? "Invalid Region" : $"{base.ToString()} Size=0x{Size.ToInt64():X} Protection={Protection}";
        }

        #endregion
        #region Equality members

        public bool Equals(RemoteRegion other)
        {
            if (other is null) return false;
            return ReferenceEquals(this, other) ||
                   Process.Equals(other.Process) &&
                   BaseAddress.Equals(other.BaseAddress) &&
                   Size.Equals(other.Size);
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is RemoteRegion other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ Size.GetHashCode();
            }
        }

        public static bool operator ==(RemoteRegion left, RemoteRegion right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(RemoteRegion left, RemoteRegion right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}
﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using MemLib.Internals;
using MemLib.Native;

namespace MemLib.Memory
{
    public sealed class MemoryManager : IDisposable
    {
        private readonly RemoteProcess m_Process;
        private readonly HashSet<RemoteAllocation> m_RemoteAllocations = new HashSet<RemoteAllocation>();
        private readonly IntPtr m_RegionStart;
        private readonly IntPtr m_RegionEnd;

        public List<RemoteAllocation> RemoteAllocations => m_RemoteAllocations.ToList();
        public IEnumerable<RemoteRegion> Regions => QueryRegions(m_RegionStart, m_RegionEnd);
        public IEnumerable<RemoteRegion> AccessibleRegions => Regions
            .Where(r => r.Protection != MemoryProtectionFlags.NoAccess && r.Protection != MemoryProtectionFlags.ZeroAccess);

        public MemoryManager(RemoteProcess process)
        {
            m_Process = process;
            m_RegionStart = new IntPtr(0x10_000);
            var is64Bit = IntPtr.Size == 8;
            m_RegionEnd = process.Is64Bit && is64Bit ? new IntPtr(0x7FFF_FFFE_0000) : new IntPtr(0x7FFE_0000);
        }

        private IEnumerable<RemoteRegion> QueryRegions(IntPtr start, IntPtr end)
        {
            return Query(m_Process.Handle, start, end).Select(region => new RemoteRegion(m_Process, region.BaseAddress, region));
        }

        #region Allocate

        public RemoteAllocation Allocate(int size, bool mustBeDisposed = true)
        {
            return Allocate(size, MemoryProtectionFlags.ExecuteReadWrite, mustBeDisposed);
        }

        public RemoteAllocation Allocate(int size, MemoryProtectionFlags protection, bool mustBeDisposed = true)
        {
            var memory = new RemoteAllocation(m_Process, size, protection, mustBeDisposed);
            m_RemoteAllocations.Add(memory);
            return memory;
        }

        #endregion
        #region Deallocate

        public void Deallocate(RemoteAllocation allocation)
        {
            if (m_RemoteAllocations.Contains(allocation))
                m_RemoteAllocations.Remove(allocation);
            if (!allocation.IsDisposed)
                allocation.Dispose();
        }

        #endregion
        #region Statics

        public static IntPtr Allocate(SafeMemoryHandle handle, int size)
        {
            return Allocate(handle, size, MemoryProtectionFlags.ExecuteReadWrite, MemoryAllocationFlags.Commit);
        }

        public static IntPtr Allocate(SafeMemoryHandle handle, int size, MemoryProtectionFlags protectionFlags)
        {
            return Allocate(handle, size, protectionFlags, MemoryAllocationFlags.Commit);
        }

        public static IntPtr Allocate(SafeMemoryHandle handle, int size, MemoryProtectionFlags protectionFlags, MemoryAllocationFlags allocationFlags)
        {
            var address = IntPtr.Zero;
            var regionSize = new IntPtr(size);
            if (NativeMethods.NtAllocateVirtualMemory(handle, ref address, 0x7FFF_FFFF, ref regionSize, allocationFlags, protectionFlags) == 0)
                return address;
            return NativeMethods.VirtualAllocEx(handle, IntPtr.Zero, new IntPtr(size), allocationFlags, protectionFlags);
        }

        public static bool Free(SafeMemoryHandle handle, IntPtr address)
        {
            return NativeMethods.VirtualFreeEx(handle, address, IntPtr.Zero, MemoryReleaseFlags.Release);
        }

        public static MemoryBasicInformation Query(SafeMemoryHandle handle, IntPtr baseAddress)
        {
            if (NativeMethods.VirtualQueryEx(handle, baseAddress, out var mbi, MarshalType<MemoryBasicInformation>.Size) == IntPtr.Zero)
                throw new Win32Exception();
            return mbi;
        }

        public static IEnumerable<MemoryBasicInformation> Query(SafeMemoryHandle handle, IntPtr start, IntPtr end)
        {
            var address = start.ToInt64();
            var limit = end.ToInt64();

            if (address >= limit)
                throw new ArgumentOutOfRangeException(nameof(start));

            IntPtr ret;
            var mbiSize = MarshalType<MemoryBasicInformation>.Size;
            do
            {
                ret = NativeMethods.VirtualQueryEx(handle, new IntPtr(address), out var mbi, mbiSize);
                address += mbi.RegionSize.ToInt64();
                if (mbi.State != MemoryStateFlags.Free && ret != IntPtr.Zero)
                    yield return mbi;
            } while (address < limit && ret != IntPtr.Zero);
        }

        #endregion
        #region IDisposable

        void IDisposable.Dispose()
        {
            foreach (var allocation in m_RemoteAllocations.Where(r => r.MustBeDisposed).ToList())
            {
                allocation.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        ~MemoryManager()
        {
            ((IDisposable)this).Dispose();
        }

        #endregion
    }
}
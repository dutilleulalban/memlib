﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Text;
using MemLib.Assembly.CallingConvention;

namespace MemLib.Memory
{
    public class RemotePointer : IEquatable<RemotePointer>
    {
        protected readonly RemoteProcess Process;
        public IntPtr BaseAddress { get; protected set; }

        public virtual bool IsValid => Process.IsRunning && BaseAddress != IntPtr.Zero;

        public RemotePointer(RemoteProcess process, IntPtr address)
        {
            Process = process;
            BaseAddress = address;
        }

        #region Overrides of Object

        public override string ToString() => $"0x{BaseAddress.ToInt64():X}";

        #endregion
        #region Read Memory

        public T Read<T>()
        {
            return Read<T>(0);
        }

        public T Read<T>(int offset)
        {
            return Process.Read<T>(BaseAddress + offset);
        }

        public T[] Read<T>(int offset, int count)
        {
            return Process.Read<T>(BaseAddress + offset, count);
        }

        public bool TryRead<T>(out T value)
        {
            return Process.TryRead(BaseAddress, out value);
        }

        public bool TryRead<T>(int offset, out T value)
        {
            return Process.TryRead(BaseAddress + offset, out value);
        }

        public bool TryRead<T>(int offset, int count, out T[] value)
        {
            return Process.TryRead(BaseAddress + offset, count, out value);
        }

        public string ReadString()
        {
            return ReadString(0, Encoding.UTF8);
        }

        public string ReadString(int offset, Encoding encoding, int maxLength = 512)
        {
            return Process.ReadString(BaseAddress + offset, encoding, maxLength);
        }

        public string ReadString(Encoding encoding, int maxLength = 512)
        {
            return ReadString(0, encoding, maxLength);
        }

        public bool TryReadString(out string value)
        {
            return TryReadString(0, out value, Encoding.UTF8);
        }

        public bool TryReadString(int offset, out string value)
        {
            return TryReadString(offset, out value, Encoding.UTF8);
        }

        public bool TryReadString(out string value, Encoding encoding, int maxLength = 512)
        {
            return TryReadString(0, out value, encoding, maxLength);
        }

        public bool TryReadString(int offset, out string value, Encoding encoding, int maxLength = 512)
        {
            return Process.TryReadString(BaseAddress + offset, out value, encoding, maxLength);
        }

        #endregion
        #region Write Memory

        public void Write<T>(T value)
        {
            Write(0, value);
        }

        public void Write<T>(int offset, T value)
        {
            Process.Write(BaseAddress + offset, value);
        }

        public void Write<T>(T[] array)
        {
            Write(0, array);
        }

        public void Write<T>(int offset, T[] array)
        {
            Process.Write(BaseAddress + offset, array);
        }

        public void WriteString(string text)
        {
            WriteString(0, text);
        }

        public void WriteString(int offset, string text, Encoding encoding)
        {
            Process.WriteString(BaseAddress + offset, text, encoding);
        }

        public void WriteString(string text, Encoding encoding)
        {
            WriteString(0, text, encoding);
        }

        public void WriteString(int offset, string text)
        {
            Process.WriteString(BaseAddress + offset, text);
        }

        #endregion
        #region Execute

        public T Exeute<T>()
        {
            return Process.Assembly.Execute<T>(BaseAddress);
        }

        public IntPtr Execute()
        {
            return Exeute<IntPtr>();
        }

        public T Execute<T>(dynamic parameter)
        {
            return Process.Assembly.Execute<T>(BaseAddress, parameter);
        }

        public IntPtr Execute(dynamic parameter)
        {
            return Execute<IntPtr>(parameter);
        }

        public T Execute<T>(CallFormat convention, params dynamic[] parameters)
        {
            return Process.Assembly.Execute<T>(BaseAddress, convention, parameters);
        }

        public IntPtr Execute(CallFormat convention, params dynamic[] parameters)
        {
            return Execute<IntPtr>(convention, parameters);
        }

        #endregion
        #region Equality members

        public bool Equals(RemotePointer other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Process, other.Process) && BaseAddress.Equals(other.BaseAddress);
        }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((RemotePointer)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Process != null ? Process.GetHashCode() : 0) * 397) ^ BaseAddress.GetHashCode();
            }
        }

        #endregion
    }
}
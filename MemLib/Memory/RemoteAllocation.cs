﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using MemLib.Native;

namespace MemLib.Memory
{
    public sealed class RemoteAllocation : RemoteRegion, IDisposable
    {
        public bool IsDisposed { get; private set; }
        public bool MustBeDisposed { get; set; }

        internal RemoteAllocation(RemoteProcess process, int size, MemoryProtectionFlags protect, bool mustBeDisposed) :
            base(process, MemoryManager.Allocate(process.Handle, size, protect))
        {
            MustBeDisposed = mustBeDisposed;
        }

        #region IDisposable

        public void Dispose()
        {
            if (IsDisposed) return;
            IsDisposed = true;
            Release();
            Process.Memory.Deallocate(this);
            BaseAddress = IntPtr.Zero;
            GC.SuppressFinalize(this);
        }

        ~RemoteAllocation()
        {
            if (MustBeDisposed)
                Dispose();
        }

        #endregion
    }
}
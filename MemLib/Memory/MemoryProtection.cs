﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.ComponentModel;
using MemLib.Native;

namespace MemLib.Memory
{
    public sealed class MemoryProtection : IDisposable, IEquatable<MemoryProtection>
    {
        private readonly RemoteProcess m_Process;
        public MemoryProtectionFlags NewProtection { get; }
        public MemoryProtectionFlags OldProtection { get; }
        public bool MustBeDisposed { get; set; }
        public IntPtr BaseAddress { get; }
        public long Size { get; }

        internal MemoryProtection(RemoteProcess process, IntPtr baseAddress, long size, MemoryProtectionFlags newProtect = MemoryProtectionFlags.ExecuteReadWrite, bool mustBeDisposed = true)
        {
            m_Process = process;
            MustBeDisposed = mustBeDisposed;
            BaseAddress = baseAddress;
            Size = size;
            NewProtection = newProtect;
            if (!NativeMethods.VirtualProtectEx(m_Process.Handle, baseAddress, new IntPtr(size), newProtect, out var oldProtect))
                throw new Win32Exception();
            OldProtection = oldProtect;
        }

        #region Overrides of Object

        public override string ToString() => $"BaseAddress=0x{BaseAddress.ToInt64():X} New={NewProtection} Old={OldProtection}";

        #endregion
        #region IDisposable

        public void Dispose()
        {
            NativeMethods.VirtualProtectEx(m_Process.Handle, BaseAddress, new IntPtr(Size), OldProtection, out _);
            GC.SuppressFinalize(this);
        }

        ~MemoryProtection()
        {
            if (MustBeDisposed)
                Dispose();
        }

        #endregion
        #region Equality members

        public bool Equals(MemoryProtection other)
        {
            if (other is null)
                return false;
            return ReferenceEquals(this, other) ||
                   m_Process.Equals(other.m_Process) &&
                   NewProtection == other.NewProtection &&
                   OldProtection == other.OldProtection &&
                   BaseAddress.Equals(other.BaseAddress) &&
                   Size == other.Size;
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is MemoryProtection other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = m_Process.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)NewProtection;
                hashCode = (hashCode * 397) ^ (int)OldProtection;
                hashCode = (hashCode * 397) ^ BaseAddress.GetHashCode();
                hashCode = (hashCode * 397) ^ Size.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(MemoryProtection left, MemoryProtection right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(MemoryProtection left, MemoryProtection right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}
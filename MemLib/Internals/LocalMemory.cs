﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Runtime.InteropServices;

namespace MemLib.Internals
{
    internal sealed class LocalMemory : IDisposable
    {
        public IntPtr Address { get; private set; }
        public int Size { get; }

        public LocalMemory(int size)
        {
            Size = size;
            Address = Marshal.AllocHGlobal(Size);
        }

        #region Read

        public T Read<T>()
        {
            return (T)Marshal.PtrToStructure(Address, typeof(T));
        }

        public byte[] Read()
        {
            var bytes = new byte[Size];
            Marshal.Copy(Address, bytes, 0, Size);
            return bytes;
        }

        #endregion

        #region Write

        public void Write(byte[] data, int index = 0)
        {
            Marshal.Copy(data, index, Address, Size);
        }

        public void Write<T>(T data)
        {
            Marshal.StructureToPtr(data, Address, false);
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Marshal.FreeHGlobal(Address);
            Address = IntPtr.Zero;
            GC.SuppressFinalize(this);
        }

        ~LocalMemory()
        {
            Dispose();
        }

        #endregion
    }
}
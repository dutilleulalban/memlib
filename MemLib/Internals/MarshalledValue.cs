﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using MemLib.Memory;

namespace MemLib.Internals
{
    internal interface IMarshalledValue : IDisposable
    {
        RemoteAllocation Allocated { get; }
        IntPtr Reference { get; }
    }

    internal static class MarshalValue
    {
        public static MarshalledValue<T> Marshal<T>(RemoteProcess proc, T value)
        {
            return new MarshalledValue<T>(proc, value);
        }
    }

    public sealed class MarshalledValue<T> : IMarshalledValue
    {
        private readonly RemoteProcess m_Process;
        public T Value { get; }
        public RemoteAllocation Allocated { get; private set; }
        public IntPtr Reference { get; private set; }

        public MarshalledValue(RemoteProcess process, T value)
        {
            m_Process = process;
            Value = value;
            Marshal();
        }

        private void Marshal()
        {
            if (typeof(T) == typeof(string))
            {
                var text = Value.ToString();
                Allocated = m_Process.Memory.Allocate(text.Length + 1);
                Allocated.WriteString(0, text);
                Reference = Allocated.BaseAddress;
            }
            else
            {
                var byteArray = MarshalType<T>.ObjectToByteArray(Value);

                if (MarshalType<T>.CanBeStoredInRegisters)
                {
                    Reference = MarshalType<IntPtr>.ByteArrayToObject(byteArray);
                }
                else
                {
                    Allocated = m_Process.Memory.Allocate(MarshalType<T>.Size);
                    Allocated.Write(0, Value);
                    Reference = Allocated.BaseAddress;
                }
            }
        }

        #region IDisposable

        public void Dispose()
        {
            Allocated?.Dispose();
            Reference = IntPtr.Zero;
            GC.SuppressFinalize(this);
        }

        ~MarshalledValue()
        {
            Dispose();
        }

        #endregion
    }
}
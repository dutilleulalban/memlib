﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MemLib.Internals;
using MemLib.Native;

namespace MemLib.Threading
{
    public sealed class RemoteThread : IDisposable, IEquatable<RemoteThread>
    {
        private readonly RemoteProcess m_Process;
        private readonly IMarshalledValue m_Parameter;
        private readonly Task m_ParameterCleaner;

        public ProcessThread Native { get; private set; }
        public SafeMemoryHandle Handle { get; }
        public int Id { get; }
        public bool IsMainThread => Equals(m_Process.Threads.MainThread);
        public bool IsAlive => !IsTerminated;
        public bool IsSuspended
        {
            get
            {
                Refresh();
                return Native != null && Native.ThreadState == ThreadState.Wait && Native.WaitReason == ThreadWaitReason.Suspended;
            }
        }
        public bool IsTerminated
        {
            get
            {
                Refresh();
                return Native == null;
            }
        }

        internal RemoteThread(RemoteProcess process, ProcessThread thread)
        {
            m_Process = process;
            Native = thread ?? throw new ArgumentNullException(nameof(thread));
            Id = thread.Id;
            Handle = ThreadManager.OpenThread(ThreadAccessFlags.AllAccess, thread.Id);
        }

        [DebuggerStepThrough]
        internal RemoteThread(RemoteProcess process, ProcessThread thread, IMarshalledValue parameter = null) : this(process, thread)
        {
            m_Parameter = parameter;
            m_ParameterCleaner = new Task(() =>
            {
                Join();
                m_Parameter?.Dispose();
            });
        }

        public void Refresh()
        {
            if (Native == null)
                return;
            m_Process.Native.Refresh();
            Native = m_Process.Threads.NativeThreads.FirstOrDefault(t => t.Id == Native.Id);
        }

        public void Join()
        {
            ThreadManager.WaitForSingleObject(Handle, 0xFFFFFFFF);
        }

        public WaitValues Join(uint timeOut)
        {
            return ThreadManager.WaitForSingleObject(Handle, timeOut);
        }

        public void Resume()
        {
            if (!IsAlive) return;
            ThreadManager.ResumeThread(Handle);
            if (m_Parameter != null && !m_ParameterCleaner.IsCompleted)
                m_ParameterCleaner.Start();
        }

        public FrozenThread Suspend()
        {
            if (!IsAlive) return null;
            ThreadManager.SuspendThread(Handle);
            return new FrozenThread(this);
        }

        public void Terminate(int exitCode = 0)
        {
            if (IsAlive)
                ThreadManager.TerminateThread(Handle, exitCode);
        }

        public T GetExitCode<T>()
        {
            var ret = ThreadManager.GetExitCodeThread(Handle);
            return ret.HasValue ? MarshalType<T>.PtrToObject(m_Process, ret.Value) : default;
        }

        #region Overrides of Object

        public override string ToString()
        {
            return $"ThreadId=0x{Id:X} IsAlive={IsAlive} IsMainThread={IsMainThread}";
        }

        #endregion
        #region IDisposable

        public void Dispose()
        {
            if (!Handle.IsClosed)
                Handle.Close();
            if (m_Parameter != null && m_Process.IsRunning)
            {
                m_ParameterCleaner.Dispose();
                m_Parameter.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        ~RemoteThread()
        {
            Dispose();
        }

        #endregion
        #region Equality members

        public bool Equals(RemoteThread other)
        {
            if (other is null) return false;
            return ReferenceEquals(this, other) || m_Process.Equals(other.m_Process) && Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is RemoteThread other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (m_Process.GetHashCode() * 397) ^ Id;
            }
        }

        public static bool operator ==(RemoteThread left, RemoteThread right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(RemoteThread left, RemoteThread right)
        {
            return !Equals(left, right);
        }

        #endregion
    }

}
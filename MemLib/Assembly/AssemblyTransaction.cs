﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using MemLib.Internals;

namespace MemLib.Assembly
{
    public sealed class AssemblyTransaction : IDisposable
    {
        private readonly RemoteProcess m_Process;
        private IntPtr m_ExitCode;
        private bool m_Executed;

        public List<string> Instructions { get; } = new List<string>();
        public IntPtr Address { get; }
        public bool IsAutoExecuted { get; set; }

        public AssemblyTransaction(RemoteProcess process, bool autoExecute) : this(process, IntPtr.Zero, autoExecute) { }
        public AssemblyTransaction(RemoteProcess process, IntPtr address, bool autoExecute)
        {
            m_Process = process;
            Address = address;
            IsAutoExecuted = autoExecute;
            m_Executed = false;
        }

        public void Add(string asm)
        {
            Instructions.Add(asm);
        }

        public void Add(IEnumerable<string> asm)
        {
            Instructions.AddRange(asm);
        }

        public byte[] Assemble()
        {
            return m_Process.Assembly.Assembler.Assemble(Instructions, Address);
        }

        public T GetExitCode<T>()
        {
            ExecuteIfNeeded();
            return MarshalType<T>.PtrToObject(m_Process, m_ExitCode);
        }

        private void ExecuteIfNeeded()
        {
            if (m_Executed)
                return;
            if (Address != IntPtr.Zero)
            {
                if (IsAutoExecuted)
                    m_ExitCode = m_Process.Assembly.InjectAndExecute(Instructions.ToArray(), Address);
                else m_Process.Assembly.Inject(Instructions.ToArray(), Address);
            }

            if (Address == IntPtr.Zero && IsAutoExecuted)
                m_ExitCode = m_Process.Assembly.InjectAndExecute(Instructions.ToArray());
            m_Executed = true;
        }

        #region Operators

        public static AssemblyTransaction operator +(AssemblyTransaction asmt, string asm)
        {
            asmt.Instructions.Add(asm);
            return asmt;
        }

        public static AssemblyTransaction operator +(AssemblyTransaction asmt, IEnumerable<string> asm)
        {
            asmt.Instructions.AddRange(asm);
            return asmt;
        }

        #endregion
        #region Overrides of Object

        public override string ToString()
        {
            return string.Join("\n", Instructions);
        }

        #endregion
        #region IDisposable

        public void Dispose()
        {
            ExecuteIfNeeded();
        }

        #endregion
    }
}
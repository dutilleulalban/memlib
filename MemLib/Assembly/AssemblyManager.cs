﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using MemLib.Assembly.Assembler;
using MemLib.Assembly.CallingConvention;
using MemLib.Internals;
using MemLib.Memory;

namespace MemLib.Assembly
{
    public sealed class AssemblyManager : IDisposable
    {
        private readonly RemoteProcess m_Process;

        public IAssembler Assembler { get; }

        internal AssemblyManager(RemoteProcess process)
        {
            m_Process = process;
            Assembler = new KeystoneAssembler(process.Is64Bit ? AssemblerMode.X64 : AssemblerMode.X32);
        }

        #region Transaction

        public AssemblyTransaction BeginTransaction(IntPtr address, bool autoExecute = true)
        {
            return new AssemblyTransaction(m_Process, address, autoExecute);
        }

        public AssemblyTransaction BeginTransaction(bool autoExecute = true)
        {
            return new AssemblyTransaction(m_Process, autoExecute);
        }

        #endregion
        #region Inject

        public bool Inject(IEnumerable<string> instructions, IntPtr address)
        {
            if (!Assembler.Assemble(instructions, address, out var buffer))
                return false;
            return m_Process.Write(address, buffer);
        }

        public RemoteAllocation Inject(IEnumerable<string> instructions, bool mustBeDisposed = true)
        {
            var insnList = instructions.ToList();
            var asm = Assembler.Assemble(insnList);
            var mem = m_Process.Memory.Allocate(asm.Length, mustBeDisposed);
            Inject(insnList, mem.BaseAddress);
            return mem;
        }

        #endregion
        #region Execute

        public T Execute<T>(IntPtr address)
        {
            var thread = m_Process.Threads.CreateAndJoin(address);
            return thread.GetExitCode<T>();
        }

        public IntPtr Execute(IntPtr address)
        {
            return Execute<IntPtr>(address);
        }

        public T Execute<T>(IntPtr address, dynamic parameter)
        {
            var thread = m_Process.Threads.CreateAndJoin(address, parameter);
            return thread.GetExitCode<T>();
        }

        public IntPtr Execute(IntPtr address, dynamic parameter)
        {
            return Execute<IntPtr>(address, parameter);
        }

        public T Execute<T>(IntPtr address, CallFormat convention, params dynamic[] parameters)
        {
            var marshalled = parameters.Select(p => MarshalValue.Marshal(m_Process, p)).Cast<IMarshalledValue>().ToArray();
            AssemblyTransaction asm;
            var format = convention;
            if (format == CallFormat.Defaut)
                format = m_Process.Is64Bit ? CallFormat.FastcallX64 : CallFormat.Cdecl;

            using (asm = BeginTransaction())
            {
                var param = marshalled.Select(p => p.Reference).ToArray();
                var call = CallBuilder.Build(format, address, param);
                asm.Add(call);
                asm.Add("ret");
            }

            foreach (var value in marshalled)
            {
                value.Dispose();
            }

            return asm.GetExitCode<T>();
        }

        public IntPtr Execute(IntPtr address, CallFormat convention, params dynamic[] parameters)
        {
            return Execute<IntPtr>(address, convention, parameters);
        }

        #endregion
        #region InjectAndExecute

        public T InjectAndExecute<T>(IEnumerable<string> instructions, IntPtr address)
        {
            Inject(instructions, address);
            return Execute<T>(address);
        }

        public IntPtr InjectAndExecute(IEnumerable<string> instructions, IntPtr address)
        {
            return InjectAndExecute<IntPtr>(instructions, address);
        }

        public T InjectAndExecute<T>(IEnumerable<string> instructions)
        {
            using (var mem = Inject(instructions))
            {
                return Execute<T>(mem.BaseAddress);
            }
        }

        public IntPtr InjectAndExecute(IEnumerable<string> instructions)
        {
            return InjectAndExecute<IntPtr>(instructions);
        }

        #endregion
        #region IDisposable

        void IDisposable.Dispose() { }

        #endregion
    }
}
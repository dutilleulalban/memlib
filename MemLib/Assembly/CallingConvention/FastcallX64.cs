﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace MemLib.Assembly.CallingConvention
{
    public partial class CallBuilder
    {
        private static IEnumerable<string> FormatFascallX64(IntPtr address, params IntPtr[] param)
        {
            var stackParams = Math.Max(param.Length - 4, 0);
            const int stackSize = 0x20;

            if (param.Length >= 1)
                yield return $"mov rcx, 0x{param[0].ToInt64():X}";
            if (param.Length >= 2)
                yield return $"mov rdx, 0x{param[1].ToInt64():X}";
            if (param.Length >= 3)
                yield return $"mov r8, 0x{param[2].ToInt64():X}";
            if (param.Length >= 4)
                yield return $"mov r9, 0x{param[3].ToInt64():X}";

            foreach (var ptr in param.Skip(4).Reverse())
            {
                var val = ptr.ToInt64();
                if (val < int.MaxValue)
                    yield return $"push 0x{val:X}";
                else
                {
                    yield return $"mov rax, 0x{val:X}";
                    yield return "push rax";
                }
            }

            yield return $"sub rsp, 0x{stackSize:X}";

            yield return $"mov rax, 0x{address.ToInt64():X}";
            yield return "call rax";

            yield return $"add rsp, 0x{stackSize + stackParams * 8:X}";
        }
    }
}
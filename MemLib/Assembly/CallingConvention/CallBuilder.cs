﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace MemLib.Assembly.CallingConvention
{
    public partial class CallBuilder
    {
        public static IEnumerable<string> Build(CallFormat call, IntPtr adddress, params IntPtr[] param)
        {
            switch (call)
            {
                case CallFormat.FastcallX64:
                    return FormatFascallX64(adddress, param);
                case CallFormat.Cdecl:
                    return FormatCdecl(adddress, param);
                case CallFormat.Stdcall:
                    return FormatStdcall(adddress, param);
                case CallFormat.Fastcall:
                    return FormatFastcallX32(adddress, param);
                case CallFormat.Thiscall:
                    return FormatThiscall(adddress, param);
                case CallFormat.GccThiscall:
                    return FormatGccThiscall(adddress, param);
                case CallFormat.Defaut:
                default:
                    throw new ArgumentOutOfRangeException(nameof(call), call, "Not supported");
            }
        }
    }
}
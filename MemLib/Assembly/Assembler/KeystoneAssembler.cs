﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using Keystone;

namespace MemLib.Assembly.Assembler
{

    public sealed class KeystoneAssembler : IAssembler
    {
        private Engine m_Engine;

        public bool ThrowOnError { get => m_Engine.ThrowOnError; set => m_Engine.ThrowOnError = value; }

        public KeystoneAssembler(AssemblerMode mode)
        {
            m_Engine = new Engine(Architecture.X86, (Mode)mode) { ThrowOnError = true };
        }

        #region IAssembler

        public byte[] Assemble(string source) => Assemble(source, IntPtr.Zero);
        public byte[] Assemble(string source, IntPtr address)
        {
            return m_Engine.Assemble(source, (ulong)address)?.Buffer;
        }

        public bool Assemble(string source, out byte[] buffer) => Assemble(source, IntPtr.Zero, out buffer);
        public bool Assemble(string source, IntPtr address, out byte[] buffer)
        {
            buffer = m_Engine.Assemble(source, (ulong)address)?.Buffer;
            return buffer != null;
        }

        public byte[] Assemble(IEnumerable<string> source) => Assemble(source, IntPtr.Zero);
        public byte[] Assemble(IEnumerable<string> source, IntPtr address)
        {
            return m_Engine.Assemble(string.Join("\n", source), (ulong)address.ToInt64())?.Buffer;
        }

        public bool Assemble(IEnumerable<string> source, out byte[] buffer) => Assemble(source, IntPtr.Zero, out buffer);
        public bool Assemble(IEnumerable<string> source, IntPtr address, out byte[] buffer)
        {
            buffer = m_Engine.Assemble(string.Join("\n", source), (ulong)address.ToInt64())?.Buffer;
            return buffer != null;
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            m_Engine?.Dispose();
            GC.SuppressFinalize(this);
        }

        ~KeystoneAssembler()
        {
            Dispose();
        }

        #endregion
    }
}
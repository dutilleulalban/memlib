﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using MemLib.Assembly;
using MemLib.Internals;
using MemLib.Memory;
using MemLib.Memory.Rtti;
using MemLib.Modules;
using MemLib.Native;
using MemLib.Threading;

namespace MemLib
{
    public class RemoteProcess : IDisposable, IEquatable<RemoteProcess>
    {
        public Process Native { get; }
        public SafeMemoryHandle Handle { get; }
        public int Id { get; }
        public string ProcessName => Native?.ProcessName;
        public bool IsRunning => Native != null && !Native.HasExited && !Handle.IsClosed && !Handle.IsInvalid;
        public bool Is64Bit { get; }
        public RemoteModule MainModule => Modules.MainModule;

        public MemoryManager Memory { get; }
        public ModuleManager Modules { get; }
        public ThreadManager Threads { get; }
        public AssemblyManager Assembly { get; }

        public event Action<RemoteProcess> Exited;

        public RemotePointer this[IntPtr address] => new RemotePointer(this, address);
        public RemotePointer this[long address] => new RemotePointer(this, new IntPtr(address));
        public RemoteModule this[string moduleName] => Modules[moduleName];

        public RemoteProcess(string processName) : this(Process.GetProcessesByName(processName).FirstOrDefault()) { }
        public RemoteProcess(Process process)
        {
            Native = process ?? throw new ArgumentNullException(nameof(process));
            Is64Bit = Is64BitProcess(process.Handle);
            if (Is64Bit && IntPtr.Size != 8)
                return;
            Handle = NativeMethods.OpenProcess(ProcessAccessFlags.AllAccess, false, process.Id);
            Id = process.Id;
            Native.Exited += NativeOnExited;
            Native.EnableRaisingEvents = true;

            Memory = new MemoryManager(this);
            Modules = new ModuleManager(this);
            Threads = new ThreadManager(this);
            Assembly = new AssemblyManager(this);
        }

        private void NativeOnExited(object sender, EventArgs e)
        {
            Exited?.Invoke(this);
        }

        #region Statics

        public static RemoteProcess FindProcess(string process)
        {
            if (Path.HasExtension(process))
                process = Path.GetFileNameWithoutExtension(process);
            var proc = Process.GetProcessesByName(process).FirstOrDefault();
            return proc == null ? null : new RemoteProcess(proc);
        }

        public static RemoteProcess GetProcessById(int processId)
        {
            try
            {
                var proc = Process.GetProcessById(processId);
                return new RemoteProcess(proc);
            }
            catch (ArgumentException)
            {
                return null;
            }
        }

        public static IEnumerable<RemoteProcess> GetProcessesByName(string processName)
        {
            return Process.GetProcessesByName(processName).Select(proc => new RemoteProcess(proc));
        }

        public static bool Is64BitProcess(IntPtr handle)
        {
            if (!Environment.Is64BitOperatingSystem) return false;
            return !(NativeMethods.IsWow64Process(handle, out var wow64) && wow64);
        }

        public static RemoteProcess GetCurrentProcess()
        {
            return new RemoteProcess(Process.GetCurrentProcess());
        }

        #endregion
        #region Rtti

        public bool TryReadRtti(IntPtr address, out RttiData rtti, bool isObjectPointer = true)
        {
            if (isObjectPointer && !TryRead(address, out address))
            {
                rtti = null;
                return false;
            }
            rtti = new RttiData(this, address);
            return true;
        }

        public RttiData ReadRtti(IntPtr address, bool isObjectPointer = true)
        {
            return TryReadRtti(address, out var rtti, isObjectPointer) ? rtti : null;
        }

        #endregion
        #region Read Memory

        [DebuggerStepThrough]
        protected virtual byte[] ReadBytes(IntPtr address, int count)
        {
            var buffer = new byte[count < 0 ? 0 : count];
            if (!NativeMethods.ReadProcessMemory(Handle, address, buffer, (IntPtr)buffer.Length, out _))
                throw new Win32Exception();
            return buffer;
        }

        [DebuggerStepThrough]
        protected virtual bool TryReadBytes(IntPtr address, int count, out byte[] buffer)
        {
            buffer = count <= 0 ? null : new byte[count];
            return buffer != null && NativeMethods.ReadProcessMemory(Handle, address, buffer, (IntPtr)buffer.Length, out _);
        }

        public T Read<T>(IntPtr address)
        {
            return MarshalType<T>.ByteArrayToObject(ReadBytes(address, MarshalType<T>.Size));
        }

        public T[] Read<T>(IntPtr address, int count)
        {
            var data = ReadBytes(address, MarshalType<T>.Size * count);
            var result = new T[count];
            if (MarshalType<T>.TypeCode != TypeCode.Byte)
                for (var i = 0; i < count; i++)
                    result[i] = MarshalType<T>.ByteArrayToObject(data, MarshalType<T>.Size * i);
            else
                Array.Copy(data, result, count);
            return result;
        }

        public bool TryRead<T>(IntPtr address, out T value)
        {
            if (TryReadBytes(address, MarshalType<T>.Size, out var buffer))
            {
                value = MarshalType<T>.ByteArrayToObject(buffer);
                return true;
            }
            value = default;
            return false;
        }

        public bool TryRead<T>(IntPtr address, int count, out T[] value)
        {
            if (count <= 0 || !TryReadBytes(address, MarshalType<T>.Size * count, out var buffer))
            {
                value = null;
                return false;
            }
            value = new T[count];
            if (MarshalType<T>.TypeCode != TypeCode.Byte)
                for (var i = 0; i < count; i++)
                    value[i] = MarshalType<T>.ByteArrayToObject(buffer, MarshalType<T>.Size * i);
            else
                Array.Copy(buffer, value, count);
            return true;
        }

        public bool Read<T>(IntPtr address, ref T[] buffer, int offset, int count)
        {
            if (count <= 0 || !TryReadBytes(address, MarshalType<T>.Size * count, out var tmp))
                return false;
            if (MarshalType<T>.TypeCode != TypeCode.Byte)
                for (var i = 0; i < count; i++)
                    buffer[offset + i] = MarshalType<T>.ByteArrayToObject(tmp, MarshalType<T>.Size * i);
            else
                Array.Copy(tmp, 0, buffer, offset, count);
            return true;
        }

        public string ReadString(IntPtr address, int maxLength = 512)
        {
            return ReadString(address, Encoding.UTF8, maxLength);
        }

        public string ReadString(IntPtr address, Encoding encoding, int maxLength = 512)
        {
            var data = encoding.GetString(ReadBytes(address, maxLength));
            var eosPos = data.IndexOf('\0');
            return eosPos == -1 ? data : data.Substring(0, eosPos);
        }

        public bool TryReadString(IntPtr address, out string value, int maxLength = 512)
        {
            return TryReadString(address, out value, Encoding.UTF8, maxLength);
        }

        public bool TryReadString(IntPtr address, out string value, Encoding encoding, int maxLength = 512)
        {
            if (!TryReadBytes(address, maxLength, out var buffer))
            {
                value = null;
                return false;
            }
            var data = encoding.GetString(buffer);
            var eosPos = data.IndexOf('\0');
            value = eosPos == -1 ? data : data.Substring(0, eosPos);
            return true;
        }

        #endregion
        #region Write Memory

        protected virtual bool WriteBytes(IntPtr address, byte[] buffer)
        {
            return NativeMethods.WriteProcessMemory(Handle, address, buffer, (IntPtr)buffer.Length, out _);
        }

        public bool Write<T>(IntPtr address, T value)
        {
            return WriteBytes(address, MarshalType<T>.ObjectToByteArray(value));
        }

        public bool Write<T>(IntPtr address, T[] array)
        {
            var size = MarshalType<T>.Size;
            var buffer = new byte[size * array.Length];
            for (var i = 0; i < array.Length; i++)
                Buffer.BlockCopy(MarshalType<T>.ObjectToByteArray(array[i]), 0, buffer, size * i, size);
            return WriteBytes(address, buffer);
        }

        public bool WriteString(IntPtr address, string text, Encoding encoding)
        {
            return WriteBytes(address, encoding.GetBytes(text + '\0'));
        }

        public bool WriteString(IntPtr address, string text)
        {
            return WriteString(address, text, Encoding.UTF8);
        }

        #endregion
        #region IDisposable

        private bool m_Disposed;

        public virtual void Dispose()
        {
            if (m_Disposed) return;
            Native.EnableRaisingEvents = false;
            Native.Exited -= NativeOnExited;
            ((IDisposable)Threads)?.Dispose();
            ((IDisposable)Memory)?.Dispose();
            ((IDisposable)Modules)?.Dispose();
            ((IDisposable)Assembly)?.Dispose();
            if (!Handle.IsClosed)
                Handle.Close();
            m_Disposed = true;
            GC.SuppressFinalize(this);
        }

        ~RemoteProcess()
        {
            Dispose();
        }

        #endregion
        #region Equality members

        public bool Equals(RemoteProcess other)
        {
            if (other is null) return false;
            return ReferenceEquals(this, other) || Id.Equals(other.Id);
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is RemoteProcess other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Native != null ? Id : 0;
        }

        public static bool operator ==(RemoteProcess left, RemoteProcess right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(RemoteProcess left, RemoteProcess right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}

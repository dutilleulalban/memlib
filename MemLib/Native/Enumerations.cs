﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;

namespace MemLib.Native
{
    #region Flags

    [Flags]
    public enum ProcessAccessFlags
    {
        AllAccess = 0x001F0FFF,
        CreateProcess = 0x0080,
        CreateThread = 0x0002,
        DupHandle = 0x0040,
        QueryInformation = 0x0400,
        QueryLimitedInformation = 0x1000,
        SetInformation = 0x0200,
        SetQuota = 0x0100,
        SuspendResume = 0x0800,
        Terminate = 0x0001,
        VmOperation = 0x0008,
        VmRead = 0x0010,
        VmWrite = 0x0020,
        Synchronize = 0x00100000
    }

    [Flags]
    public enum MemoryAllocationFlags
    {
        Commit = 0x00001000,
        Reserve = 0x00002000,
        Reset = 0x00080000,
        ResetUndo = 0x1000000,
        LargePages = 0x20000000,
        Physical = 0x00400000,
        TopDown = 0x00100000
    }

    [Flags]
    public enum MemoryProtectionFlags
    {
        ZeroAccess = 0x0,
        Execute = 0x10,
        ExecuteRead = 0x20,
        ExecuteReadWrite = 0x40,
        ExecuteWriteCopy = 0x80,
        NoAccess = 0x01,
        ReadOnly = 0x02,
        ReadWrite = 0x04,
        WriteCopy = 0x08,
        Guard = 0x100,
        NoCache = 0x200,
        WriteCombine = 0x400
    }

    [Flags]
    public enum MemoryReleaseFlags
    {
        Decommit = 0x4000,
        Release = 0x8000
    }

    [Flags]
    public enum MemoryStateFlags
    {
        Commit = 0x1000,
        Free = 0x10000,
        Reserve = 0x2000
    }

    [Flags]
    public enum MemoryTypeFlags
    {
        None = 0x0,
        Image = 0x1000000,
        Mapped = 0x40000,
        Private = 0x20000
    }

    [Flags]
    public enum UnDecorateFlags : uint
    {
        Complete = 0x0000,
        NoLeadingUnderscores = 0x0001,
        NoMsKeywords = 0x0002,
        NoFunctionReturns = 0x0004,
        NoAllocationModel = 0x0008,
        NoAllocationLanguage = 0x0010,
        NoMsThisType = 0x0020,
        NoCvThisType = 0x0040,
        NoThisType = 0x0060,
        NoAccessSpecifiers = 0x0080,
        NoThrowSignatures = 0x0100,
        NoMemberType = 0x0200,
        NoReturnUdtModel = 0x0400,

        //_32BitDecode = 0x0800,
        NameOnly = 0x1000,
        NoArguments = 0x2000,
        NoSpecialSyms = 0x4000
    }

    [Flags]
    public enum ThreadAccessFlags
    {
        Synchronize = 0x00100000,
        AllAccess = 0x001F0FFF,
        DirectImpersonation = 0x0200,
        GetContext = 0x0008,
        Impersonate = 0x0100,
        QueryInformation = 0x0040,
        QueryLimitedInformation = 0x0800,
        SetContext = 0x0010,
        SetInformation = 0x0020,
        SetLimitedInformation = 0x0400,
        SetThreadToken = 0x0080,
        SuspendResume = 0x0002,
        Terminate = 0x0001
    }

    [Flags]
    public enum ThreadCreationFlags
    {
        Run = 0x0,
        Suspended = 0x04,
        StackSizeParamIsAReservation = 0x10000
    }

    [Flags]
    public enum Th32CsFlags : uint
    {
        Snapheaplist = 0x00000001,
        Snapprocess = 0x00000002,
        Snapthread = 0x00000004,
        Snapmodule = 0x00000008,
        Snapmodule32 = 0x00000010,
        Inherit = 0x80000000,
        Snapall = Snapheaplist | Snapmodule | Snapprocess | Snapthread
    }

    [Flags]
    public enum EnumModulesFlags
    {
        ListDefault = 0,
        List32Bit = 0x01,
        List64Bit = 0x02,
        ListAll = List32Bit | List64Bit
    }

    [Flags]
    public enum WindowStylesEx : uint
    {
        Acceptfiles = 0x00000010,
        Appwindow = 0x00040000,
        Clientedge = 0x00000200,
        Composited = 0x02000000,
        Contexthelp = 0x00000400,
        Controlparent = 0x00010000,
        Dlgmodalframe = 0x00000001,
        Layered = 0x00080000,
        Layoutrtl = 0x00400000,
        Left = 0x00000000,
        Leftscrollbar = 0x00004000,
        Ltrreading = 0x00000000,
        Mdichild = 0x00000040,
        Noactivate = 0x08000000,
        Noinheritlayout = 0x00100000,
        Noparentnotify = 0x00000004,
        Noredirectionbitmap = 0x00200000,
        Overlappedwindow = Windowedge | Clientedge,
        Palettewindow = Windowedge | Toolwindow | Topmost,
        Right = 0x00001000,
        Rightscrollbar = 0x00000000,
        Rtlreading = 0x00002000,
        Staticedge = 0x00020000,
        Toolwindow = 0x00000080,
        Topmost = 0x00000008,
        Transparent = 0x00000020,
        Windowedge = 0x00000100
    }

    [Flags]
    public enum WindowStyles : uint
    {
        Border = 0x00800000,
        Caption = 0x00C00000,
        Child = 0x40000000,
        Childwindow = 0x40000000,
        Clipchildren = 0x02000000,
        Clipsiblings = 0x04000000,
        Disabled = 0x08000000,
        Dlgframe = 0x00400000,
        Group = 0x00020000,
        Hscroll = 0x00100000,
        Iconic = 0x20000000,
        Maximize = 0x01000000,
        Maximizebox = 0x00010000,
        Minimize = 0x20000000,
        Minimizebox = 0x00020000,
        Overlapped = 0x00000000,
        Overlappedwindow = Overlapped | Caption | Sysmenu | Thickframe | Minimizebox | Maximizebox,
        Popup = 0x80000000,
        Popupwindow = Popup | Border | Sysmenu,
        Sizebox = 0x00040000,
        Sysmenu = 0x00080000,
        Tabstop = 0x00010000,
        Thickframe = 0x00040000,
        Tiled = 0x00000000,
        Tiledwindow = Overlapped | Caption | Sysmenu | Thickframe | Minimizebox | Maximizebox,
        Visible = 0x10000000,
        Vscroll = 0x00200000
    }

    #endregion

    #region Enums

    public enum WaitValues : uint
    {
        Abandoned = 0x80,
        Signaled = 0x0,
        Timeout = 0x102,
        Failed = 0xFFFFFFFF
    }

    public enum GwlIndex
    {
        GwlExstyle = -20,
        GwlpHinstance = -6,
        GwlpHwndparent = -8,
        GwlpId = -12,
        GwlStyle = -16,
        GwlpUserdata = -21,
        GwlpWndproc = -4
    }

    #endregion
}
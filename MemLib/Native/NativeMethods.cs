﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace MemLib.Native
{
    public static class NativeMethods
    {
        #region Kernel32

        [DllImport("kernel32", SetLastError = true)]
        public static extern SafeMemoryHandle OpenProcess(ProcessAccessFlags dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32", SetLastError = true)]
        public static extern bool CloseHandle(SafeMemoryHandle hObject);

        [DllImport("kernel32")]
        public static extern int GetCurrentProcessId();

        [DllImport("kernel32", SetLastError = true)]
        public static extern bool QueryFullProcessImageName(SafeMemoryHandle hProcess, int dwFlags, StringBuilder lpExeName, ref int lpdwSize);


        [DllImport("kernel32", SetLastError = true)]
        public static extern bool ReadProcessMemory(SafeMemoryHandle hProcess, IntPtr lpBaseAddress, [Out] byte[] lpBuffer, IntPtr nSize, out IntPtr lpNumberOfBytesRead);

        [DllImport("kernel32", SetLastError = true)]
        public static extern bool WriteProcessMemory(SafeMemoryHandle hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, IntPtr nSize, out IntPtr lpNumberOfBytesWritten);


        [DllImport("kernel32", SetLastError = true)]
        public static extern IntPtr VirtualAllocEx(SafeMemoryHandle hProcess, IntPtr lpAddress, IntPtr dwSize, MemoryAllocationFlags flAllocationType, MemoryProtectionFlags flProtect);

        [DllImport("kernel32", SetLastError = true)]
        public static extern bool VirtualFreeEx(SafeMemoryHandle hProcess, IntPtr lpAddress, IntPtr dwSize, MemoryReleaseFlags dwFreeType);

        [DllImport("kernel32", SetLastError = true)]
        public static extern bool VirtualProtectEx(SafeMemoryHandle hProcess, IntPtr lpAddress, IntPtr dwSize, MemoryProtectionFlags flNewProtect, out MemoryProtectionFlags lpflOldProtect);

        [DllImport("kernel32", SetLastError = true)]
        public static extern IntPtr VirtualQueryEx(SafeMemoryHandle hProcess, IntPtr lpAddress, out MemoryBasicInformation lpBuffer, int dwLength);


        [DllImport("kernel32", SetLastError = true)]
        public static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);

        [DllImport("kernel32", SetLastError = false)]
        public static extern void GetNativeSystemInfo(out SystemInfo lpSystemInfo);


        [DllImport("kernel32", SetLastError = true)]
        public static extern SafeMemoryHandle CreateRemoteThread(SafeMemoryHandle hProcess, IntPtr lpThreadAttributes, uint dwStackSize, IntPtr lpStartAddress, IntPtr lpParameter, ThreadCreationFlags dwCreationFlags, out uint lpThreadId);

        [DllImport("kernel32", SetLastError = true)]
        public static extern SafeMemoryHandle OpenThread(ThreadAccessFlags dwDesiredAccess, bool bInheritHandle, int dwThreadId);

        [DllImport("kernel32", SetLastError = true)]
        public static extern uint ResumeThread(SafeMemoryHandle hThread);

        [DllImport("kernel32", SetLastError = true)]
        public static extern uint SuspendThread(SafeMemoryHandle hThread);

        [DllImport("kernel32", SetLastError = true)]
        public static extern bool TerminateThread(SafeMemoryHandle hThread, int dwExitCode);

        [DllImport("kernel32", SetLastError = true)]
        public static extern bool GetExitCodeThread(SafeMemoryHandle hThread, out IntPtr lpExitCode);

        [DllImport("kernel32", SetLastError = true)]
        public static extern WaitValues WaitForSingleObject(SafeMemoryHandle hHandle, uint dwMilliseconds);

        [DllImport("kernel32", SetLastError = true)]
        public static extern bool GetThreadTimes(SafeMemoryHandle hThread, out long lpCreationTime, out long lpExitTime, out long lpKernelTime, out long lpUserTime);

        [DllImport("kernel32", SetLastError = true)]
        public static extern int GetThreadId(SafeMemoryHandle hThread);


        //[DllImport("kernel32", SetLastError = true)]
        //public static extern IntPtr CreateToolhelp32Snapshot(Th32CsFlags dwFlags, int th32ProcessId);

        //[DllImport("kernel32", SetLastError = true)]
        //public static extern bool Process32First(SafeMemoryHandle hSnapshot, WinProcessEntry lppe);

        //[DllImport("kernel32", SetLastError = true)]
        //public static extern bool Process32Next(SafeMemoryHandle hSnapshot, WinProcessEntry lppe);

        //[DllImport("kernel32", SetLastError = true)]
        //public static extern bool Thread32First(SafeMemoryHandle hSnapshot, WinThreadEntry lpte);

        //[DllImport("kernel32", SetLastError = true)]
        //public static extern bool Thread32Next(SafeMemoryHandle hSnapshot, WinThreadEntry lpte);

        //[DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        //public static extern bool Module32First(SafeMemoryHandle hSnapshot, WinModuleEntry lpme);

        //[DllImport("kernel32", SetLastError = true)]
        //public static extern bool Module32Next(SafeMemoryHandle hSnapshot, WinModuleEntry lpme);

        //[DllImport("kernel32", SetLastError = false)]
        //public static extern bool Toolhelp32ReadProcessMemory(uint th32ProcessId, IntPtr lpBaseAddress, [Out] byte[] lpBuffer, IntPtr cbRead, out IntPtr lpNumberOfBytesRead);


        //[DllImport("kernel32", SetLastError = false)]
        //public static extern IntPtr GetCurrentProcess();

        //[DllImport("kernel32", SetLastError = true)]
        //public static extern bool GetProcessTimes(SafeMemoryHandle hProcess, out long lpCreationTime, out long lpExitTime, out long lpKernelTime, out long lpUserTime);

        #endregion

        #region NtDll

        [DllImport("ntdll", SetLastError = false)]
        public static extern uint NtAllocateVirtualMemory(SafeMemoryHandle processHandle, [In, Out] ref IntPtr baseAddress, uint zeroBits, [In, Out] ref IntPtr regionSize, MemoryAllocationFlags allocationType, MemoryProtectionFlags protect);

        [DllImport("ntdll")]
        public static extern uint NtQueryInformationThread(SafeMemoryHandle threadHandle, uint infoclass, ref ThreadBasicInformation threadinfo, int length, IntPtr bytesread);

        #endregion

        #region Psapi

        [DllImport("psapi", SetLastError = true)]
        public static extern bool EnumProcessModules(SafeMemoryHandle hProcess, [MarshalAs(UnmanagedType.LPArray), In, Out] IntPtr[] lphModule, int cb, out int lpcbNeeded);

        [DllImport("psapi", SetLastError = true)]
        public static extern bool EnumProcessModulesEx(SafeMemoryHandle hProcess, [MarshalAs(UnmanagedType.LPArray), In, Out] IntPtr[] lphModule, int cb, out int lpcbNeeded, EnumModulesFlags dwFilterFlag);

        [DllImport("psapi", SetLastError = true)]
        public static extern int GetModuleFileNameEx(SafeMemoryHandle hProcess, IntPtr hModule, StringBuilder lpFilename, int nSize);

        [DllImport("psapi", SetLastError = true)]
        public static extern int GetModuleBaseName(SafeMemoryHandle hProcess, IntPtr hModule, StringBuilder lpBaseName, int nSize);

        [DllImport("psapi", SetLastError = true)]
        public static extern bool GetModuleInformation(SafeMemoryHandle hProcess, IntPtr hModule, ref NtModuleInfo lpmodinfo, int cb);

        [DllImport("psapi", SetLastError = true)]
        public static extern int GetProcessImageFileName(SafeMemoryHandle hProcess, StringBuilder lpImageFileName, int nSize);

        [DllImport("psapi", SetLastError = true)]
        public static extern bool EnumProcesses([MarshalAs(UnmanagedType.LPArray), In, Out] int[] lpidProcess, int cb, out int lpcbNeeded);

        #endregion

        #region User32

        [DllImport("user32", SetLastError = true)]
        public static extern int GetWindowLongPtr(IntPtr hWnd, GwlIndex nIndex);

        [DllImport("user32", SetLastError = true)]
        public static extern int SetWindowLongPtr(IntPtr hWnd, GwlIndex nIndex, int dwNewLong);

        #endregion

        #region dbghelp

        [DllImport("dbghelp", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool UnDecorateSymbolName(string name, StringBuilder outputString, int maxStringLength, UnDecorateFlags flags);

        #endregion
    }
}
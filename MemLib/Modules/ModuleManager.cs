﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using MemLib.Native;

namespace MemLib.Modules
{
    public sealed class ModuleManager : IDisposable
    {
        private readonly RemoteProcess m_Process;
        private readonly HashSet<InjectedModule> m_InjectedModules = new HashSet<InjectedModule>();
        internal IEnumerable<ProcessModule> NativeModules => m_Process.Native.Modules.Cast<ProcessModule>();
        public List<RemoteModule> RemoteModules => NativeModules.Select(m => new RemoteModule(m_Process, m)).ToList();
        public RemoteModule MainModule { get; }

        public RemoteModule this[string moduleName] => FetchModule(moduleName);

        public ModuleManager(RemoteProcess process)
        {
            m_Process = process;
            MainModule = new RemoteModule(process, process.Native.MainModule);
        }

        private RemoteModule FetchModule(string moduleName)
        {
            if (!Path.HasExtension(moduleName))
                moduleName += ".dll";
            var nativeMod = NativeModules.FirstOrDefault(m => m.ModuleName.Equals(moduleName, StringComparison.OrdinalIgnoreCase));
            return nativeMod == null ? null : new RemoteModule(m_Process, nativeMod);
        }

        #region Inject

        public InjectedModule Inject(string moduleFile, bool mustBeDisposed = true)
        {
            if (!File.Exists(moduleFile))
                throw new FileNotFoundException("File not found.", moduleFile);
            var module = InternalInject(moduleFile, mustBeDisposed);
            if (module != null && !m_InjectedModules.Contains(module))
                m_InjectedModules.Add(module);
            return module;
        }

        private InjectedModule InternalInject(string path, bool mustBeDisposed)
        {
            var thread = m_Process.Threads.CreateAndJoin(m_Process["kernel32"]["LoadLibraryA"].BaseAddress, path);
            var exitCode = thread.GetExitCode<int>();
            if (exitCode == 0) return null;
            var moduleName = Path.GetFileName(path);
            var nativeMod = NativeModules.FirstOrDefault(m => m.ModuleName.Equals(moduleName, StringComparison.OrdinalIgnoreCase));
            return nativeMod == null ? null : new InjectedModule(m_Process, nativeMod, mustBeDisposed);
        }

        #endregion
        #region Eject

        public void Eject(RemoteModule module)
        {
            if (!module.IsValid) return;

            var injected = m_InjectedModules.FirstOrDefault(m => m.Equals(module));
            if (injected != null)
                m_InjectedModules.Remove(injected);

            InternalEject(module);
        }

        public void Eject(string moduleName)
        {
            var module = RemoteModules.FirstOrDefault(m => m.ModuleName.Equals(moduleName, StringComparison.OrdinalIgnoreCase));
            if (module != null)
                InternalEject(module);
        }

        private void InternalEject(RemoteModule module)
        {
            m_Process.Threads.CreateAndJoin(m_Process["kernel32"]["FreeLibrary"].BaseAddress, module.BaseAddress);
            var name = module.ModuleName;
            foreach (var key in RemoteModule.FunctionCache.Keys)
            {
                if (key.Item2.Equals(m_Process.Handle) && key.Item3.Equals(name))
                    RemoteModule.FunctionCache.TryRemove(key, out _);
            }
        }

        #endregion
        #region Statics

        public static string UnDecorateSymbolName(string name, UnDecorateFlags flags = UnDecorateFlags.NameOnly)
        {
            var sb = new StringBuilder(260);
            return NativeMethods.UnDecorateSymbolName(name, sb, sb.Capacity, flags) ? sb.ToString() : name;
        }

        public static string GetProcessMainModuleName(SafeMemoryHandle hProcess)
        {
            var mainModuleName = new StringBuilder(1024);
            var length = mainModuleName.Capacity;
            if (!NativeMethods.QueryFullProcessImageName(hProcess, 0, mainModuleName, ref length))
                throw new Win32Exception();
            return Path.GetFileName(mainModuleName.ToString());
        }

        public static IntPtr[] GetProcessModuleHandles(SafeMemoryHandle hProcess, bool mainModuleOnly = false)
        {
            if (hProcess.IsInvalid || hProcess.IsClosed)
                return Array.Empty<IntPtr>();
            if (mainModuleOnly)
            {
                var hMain = new IntPtr[1];
                if (!NativeMethods.EnumProcessModulesEx(hProcess, hMain, IntPtr.Size, out _, EnumModulesFlags.ListAll))
                    return Array.Empty<IntPtr>();
                return hMain;
            }
            var hModules = new IntPtr[0];
            if (!NativeMethods.EnumProcessModulesEx(hProcess, hModules, 0, out var sizeNeeded, EnumModulesFlags.ListAll))
                return Array.Empty<IntPtr>();
            var numberOfModules = sizeNeeded / IntPtr.Size;
            hModules = new IntPtr[numberOfModules];
            if (!NativeMethods.EnumProcessModulesEx(hProcess, hModules, sizeNeeded, out _, EnumModulesFlags.ListAll))
                return Array.Empty<IntPtr>();
            return hModules;
        }

        #endregion
        #region IDisposable

        void IDisposable.Dispose()
        {
            foreach (var module in m_InjectedModules.Where(m => m.MustBeDisposed).ToList())
            {
                module.Dispose();
            }
            foreach (var key in RemoteModule.FunctionCache.Keys)
            {
                if (key.Item2.Equals(m_Process.Handle))
                    RemoteModule.FunctionCache.TryRemove(key, out _);
            }
            GC.SuppressFinalize(this);
        }

        ~ModuleManager()
        {
            ((IDisposable)this).Dispose();
        }

        #endregion
    }
}
﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Text;
using MemLib.Memory;
using MemLib.Modules.PeHeader.Structures;

namespace MemLib.Modules.PeHeader
{
    public sealed class PeFileHeader : RemotePointer
    {
        private bool? m_Is64Bit;
        public bool Is64Bit => m_Is64Bit ?? (m_Is64Bit = Read<ushort>((int)DosHeader.e_lfanew + 0x04) == 0x8664).Value;
        public ImageDosHeader DosHeader { get; }
        public ImageNtHeader NtHeader { get; }
        public ImageExportDirectory ExportDirectory { get; }
        public ImageSectionHeaders SectionHeaders { get; }
        public IEnumerable<ExportFunction> ExportFunctions => ReadExports();

        public PeFileHeader(RemoteProcess process, IntPtr moduleBase) : base(process, moduleBase)
        {
            DosHeader = new ImageDosHeader(process, moduleBase);
            NtHeader = new ImageNtHeader(process, moduleBase + (int)DosHeader.e_lfanew, Is64Bit);

            var numSections = (int)NtHeader.FileHeader.NumberOfSections;
            var secHeaderStart = DosHeader.e_lfanew + NtHeader.FileHeader.SizeOfOptionalHeader + 0x18;
            SectionHeaders = new ImageSectionHeaders(process, moduleBase + (int)secHeaderStart, numSections);

            var exportDirVa = NtHeader.OptionalHeader.DataDirectory[0].VirtualAddress;
            if (exportDirVa != 0)
            {
                ExportDirectory = new ImageExportDirectory(process, moduleBase + (int)exportDirVa);
            }
        }

        private IEnumerable<ExportFunction> ReadExports()
        {
            if (ExportDirectory == null || ExportDirectory.NumberOfFunctions <= 0)
                yield break;

            var numberOfNames = ExportDirectory.NumberOfNames;
            if (numberOfNames <= 0)
                yield break;

            var exportBase = ExportDirectory.Base;
            var funcAddresses = Read<int>(ExportDirectory.AddressOfFunctions, numberOfNames);
            var ordinals = Read<ushort>(ExportDirectory.AddressOfNameOrdinals, numberOfNames);
            var nameAddresses = Read<int>(ExportDirectory.AddressOfNames, numberOfNames);

            for (var i = 0; i < numberOfNames; i++)
            {
                var ordinal = i + exportBase;
                var ordinalIndex = ordinals[i];
                var rva = ordinalIndex < numberOfNames ? funcAddresses[ordinalIndex] : 0;
                if (rva <= 0)
                    continue;
                var nameAddr = nameAddresses[i];
                var name = nameAddr == 0 ? string.Empty : ReadString(nameAddr, Encoding.UTF8);
                yield return new ExportFunction(name, rva, ordinal);
            }
        }
    }
}
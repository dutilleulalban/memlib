﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using MemLib.Memory;

namespace MemLib.Modules.PeHeader.Structures
{
    public sealed class ImageFileHeader : RemotePointer
    {
        public FileHeaderMachine Machine => (FileHeaderMachine)Read<ushort>(0x00);
        public ushort NumberOfSections => Read<ushort>(0x02);
        public uint TimeDateStamp => Read<uint>(0x04);
        public uint NumberOfSymbols => Read<uint>(0x0C);
        public ushort SizeOfOptionalHeader => Read<ushort>(0x10);
        public FileHeaderCharacteristics Characteristics => (FileHeaderCharacteristics)Read<ushort>(0x12);

        public ImageFileHeader(RemoteProcess process, IntPtr address) : base(process, address) { }
    }
}
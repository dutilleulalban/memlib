﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using MemLib.Memory;

namespace MemLib.Modules.PeHeader.Structures
{
    public sealed class ImageExportDirectory : RemotePointer
    {
        public uint Characteristics => Read<uint>(0x00);
        public uint TimeDateStamp => Read<uint>(0x04);
        public ushort MajorVersion => Read<ushort>(0x08);
        public ushort MinorVersion => Read<ushort>(0x0A);
        public uint Name => Read<uint>(0x0C);
        public int Base => Read<int>(0x10);
        public int NumberOfFunctions => Read<int>(0x14);
        public int NumberOfNames => Read<int>(0x18);
        public int AddressOfFunctions => Read<int>(0x1C);
        public int AddressOfNames => Read<int>(0x20);
        public int AddressOfNameOrdinals => Read<int>(0x24);

        public ImageExportDirectory(RemoteProcess process, IntPtr address) : base(process, address) { }
    }
}
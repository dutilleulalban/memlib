﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Text;
using MemLib.Memory;

namespace MemLib.Modules.PeHeader.Structures
{
    public sealed class ImageSectionHeader : RemotePointer
    {
        public string Name => ReadString(0x00, Encoding.UTF8, 8);
        public uint PhysicalAddress => Read<uint>(0x08);
        public uint VirtualSize => PhysicalAddress;
        public uint VirtualAddress => Read<uint>(0x0C);
        public uint SizeOfRawData => Read<uint>(0x10);
        public uint PointerToRawData => Read<uint>(0x14);
        public uint PointerToRelocations => Read<uint>(0x18);
        public uint PointerToLinenumbers => Read<uint>(0x1C);
        public ushort NumberOfRelocations => Read<ushort>(0x20);
        public ushort NumberOfLinenumbers => Read<ushort>(0x22);
        public uint Characteristics => Read<uint>(0x24);

        public ImageSectionHeader(RemoteProcess proc, IntPtr address) : base(proc, address) { }
    }
}
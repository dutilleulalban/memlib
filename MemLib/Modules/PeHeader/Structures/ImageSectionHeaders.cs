﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using MemLib.Memory;

namespace MemLib.Modules.PeHeader.Structures
{
    public sealed class ImageSectionHeaders : RemotePointer
    {
        private readonly int m_SectionsMax;

        public IEnumerable<ImageSectionHeader> All
        {
            get
            {
                for (var i = 0; i < m_SectionsMax; i++)
                    yield return GetSectionHeader(i);
            }
        }

        public ImageSectionHeader this[int index] => GetSectionHeader(index);


        public ImageSectionHeaders(RemoteProcess process, IntPtr address, int numSections) : base(process, address)
        {
            m_SectionsMax = numSections;
        }

        private ImageSectionHeader GetSectionHeader(int index)
        {
            return new ImageSectionHeader(Process, BaseAddress + index * 0x28);
        }
    }
}
﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MemLib.Memory;
using MemLib.Modules.PeHeader;
using MemLib.Native;

namespace MemLib.Modules
{
    public class RemoteModule : RemoteRegion
    {
        private PeFileHeader m_PeHeader;
        internal static ConcurrentDictionary<Tuple<string, SafeMemoryHandle, string>, RemoteFunction> FunctionCache = new ConcurrentDictionary<Tuple<string, SafeMemoryHandle, string>, RemoteFunction>();
        public ProcessModule Native { get; }
        public PeFileHeader PeHeader => m_PeHeader ?? (m_PeHeader = new PeFileHeader(Process, BaseAddress));
        public string ModuleName => Native.ModuleName;
        public string FileName => Native.FileName;
        public override IntPtr Size => new IntPtr(Native.ModuleMemorySize);
        public bool IsMainModule => Process.MainModule.BaseAddress == BaseAddress;

        public IEnumerable<RemoteFunction> Exports => PeHeader.ExportFunctions.Select(ExportToRemote);

        public override bool IsValid => base.IsValid && Process.Modules.NativeModules.Any(m => m.BaseAddress == BaseAddress && m.ModuleName == ModuleName);

        public RemoteFunction this[string functionName] => FindFunction(functionName);

        internal RemoteModule(RemoteProcess process, ProcessModule module) : base(process, module.BaseAddress)
        {
            Native = module;
        }

        private RemoteFunction ExportToRemote(ExportFunction func)
        {
            return new RemoteFunction(Process, BaseAddress + func.RelativeAddress, func.Name);
        }

        public void Eject()
        {
            Process.Modules.Eject(this);
            BaseAddress = IntPtr.Zero;
        }

        public RemoteFunction FindFunction(string functionName, bool compareUnDecorated = false)
        {
            var tuple = new Tuple<string, SafeMemoryHandle, string>(functionName, Process.Handle, ModuleName);
            if (FunctionCache.ContainsKey(tuple))
                return FunctionCache[tuple];
            var function = Exports.FirstOrDefault(f => f.Name.Equals(functionName) || compareUnDecorated && f.UndecoratedName.Equals(functionName));
            if (function != null)
                FunctionCache.TryAdd(tuple, function);
            return function;
        }

        #region Overrides of RemoteRegion

        public override string ToString()
        {
            return $"BaseAddress=0x{BaseAddress.ToInt64():X} Size=0x{Size.ToInt64():X} Name={ModuleName}";
        }

        #endregion
    }
}
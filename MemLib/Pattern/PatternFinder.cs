﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemLib.Memory;
using MemLib.Native;

namespace MemLib.Pattern
{
    public class PatternFinder
    {
        private readonly RemoteProcess m_Process;

        public SearchSettings Settings { get; }

        public PatternFinder(RemoteProcess process)
        {
            m_Process = process;
            Settings = SearchSettings.Default();
        }

        public PatternFinder(RemoteProcess process, SearchSettings settings)
        {
            m_Process = process;
            Settings = settings;
        }

        public PatternFinder(RemoteProcess process, string moduleName)
        {
            m_Process = process;
            var module = process.Modules[moduleName];
            if (module == null)
                throw new ArgumentException($"Module '{moduleName}' not found.", nameof(moduleName));
            Settings = SearchSettings.Module(moduleName);
        }

        public IntPtr Find(string patternString, SearchSettings settings)
        {
            return FindMany(patternString, settings, true).FirstOrDefault();
        }

        public IntPtr Find(string patternString)
        {
            return FindMany(patternString, Settings, true).FirstOrDefault();
        }

        public IntPtr Find(string patternString, byte[] data, IntPtr baseAddress)
        {
            return FindMany(patternString, data, baseAddress, true).FirstOrDefault();
        }

        public List<IntPtr> FindMany(string patternString, bool matchFirstOnly = false)
        {
            return FindMany(patternString, Settings, matchFirstOnly);
        }

        public List<IntPtr> FindMany(string patternString, byte[] data, IntPtr baseAddress, bool matchFirstOnly = false)
        {
            var pattern = PatternParser.Parse(patternString);
            var results = FindManyInBuffer(PatternParser.Parse(patternString), data, matchFirstOnly).Select(r => baseAddress + r);
            return results.Select(r => pattern.ApplyFuncs(m_Process, r)).Where(r => r != IntPtr.Zero).ToList();
        }

        public List<IntPtr> FindMany(string patternString, SearchSettings settings, bool matchFirstOnly = false)
        {
            var pattern = PatternParser.Parse(patternString);
            var results = FindMany(pattern, settings, matchFirstOnly);
            return results.Select(r => pattern.ApplyFuncs(m_Process, r)).Where(r => r != IntPtr.Zero).ToList();
        }

        private static IEnumerable<int> FindManyInBuffer(SearchPattern pattern, byte[] buffer, bool matchFirstOnly)
        {
            if (buffer.Length == 0 || buffer.Length < pattern.Length)
                return new List<int>();
            return pattern.Search(buffer, 0, matchFirstOnly);
        }

        private IEnumerable<IntPtr> FindManyInRegion(SearchPattern pattern, RemoteRegion region, bool matchFirstOnly)
        {
            if (region == null || !m_Process.TryRead<byte>(region.BaseAddress, region.Size.ToInt32(), out var data))
                return new List<IntPtr>();
            return pattern
                .Search(data, 0, matchFirstOnly)
                .Select(val => region.BaseAddress + val);
        }

        private IEnumerable<IntPtr> FindMany(SearchPattern pattern, SearchSettings settings, bool matchFirstOnly)
        {
            var regions = GetSearchableRegions(settings);
            if (regions.Count == 0) return new List<IntPtr>();
            if (regions.Count == 1) return FindManyInRegion(pattern, regions.FirstOrDefault(), matchFirstOnly);
            var combinedRegions = CombineRegions(regions);
            var results = new ConcurrentBag<IntPtr>();
            Parallel.ForEach(combinedRegions, (tuple, state) =>
            {
                tuple.Deconstruct(out var start, out var size);
                var end = start + size;

                if (settings.SearchType == SearchType.AddressRange && IsInRange(settings.StartAddress, start, end))
                {
                    size -= IntPtrSub(settings.StartAddress, start).ToInt32();
                    start = settings.StartAddress;
                }

                if (settings.SearchType == SearchType.AddressRange && IsInRange(settings.StopAddress, start, end))
                    size -= IntPtrSub(end, settings.StopAddress).ToInt32();

                if (state.IsStopped || !m_Process.TryRead<byte>(start, size, out var data))
                    return;
                foreach (var match in pattern.Search(data, 0, matchFirstOnly))
                {
                    if (state.IsStopped) break;
                    results.Add(start + match);
                    if (!matchFirstOnly)
                        continue;
                    state.Stop();
                    break;
                }
            });
            return results;
        }

        private List<RemoteRegion> GetSearchableRegions(SearchSettings settings)
        {
            switch (settings.SearchType)
            {
                case SearchType.MainModule:
                    return new List<RemoteRegion> { m_Process.MainModule };
                case SearchType.AllModules:
                    return new List<RemoteRegion>(m_Process.Modules.RemoteModules);
                case SearchType.ModuleByName:
                    {
                        var module = m_Process.Modules[settings.TargetModule];
                        return module == null ? new List<RemoteRegion>() : new List<RemoteRegion> { module };
                    }
                case SearchType.AddressRange:
                    return m_Process.Memory.AccessibleRegions.Where(r => !r.Protection.HasFlag(MemoryProtectionFlags.Guard))
                        .Where(r => IsInRange(r.BaseAddress, settings.StartAddress, settings.StopAddress)
                                    || IsInRange(settings.StartAddress, r.BaseAddress, r.BaseAddress + r.Size.ToInt32())
                                    || IsInRange(settings.StopAddress, r.BaseAddress, r.BaseAddress + r.Size.ToInt32())).ToList();
                case SearchType.AllMemoryRegions:
                    return m_Process.Memory.AccessibleRegions.Where(r => !r.Protection.HasFlag(MemoryProtectionFlags.Guard)).ToList();
                default:
                    return new List<RemoteRegion>();
            }
        }

        private static IEnumerable<Tuple<IntPtr, int>> CombineRegions(IList<RemoteRegion> regions)
        {
            if (regions.Count <= 0)
                yield break;
            var address = regions[0].BaseAddress;
            var size = regions[0].Size.ToInt32();
            for (var i = 1; i < regions.Count; ++i)
            {
                var region = regions[i];
                if (region.BaseAddress == address + size)
                {
                    size += region.Size.ToInt32();
                }
                else
                {
                    yield return new Tuple<IntPtr, int>(address, size);
                    address = region.BaseAddress;
                    size = region.Size.ToInt32();
                }
            }
            yield return new Tuple<IntPtr, int>(address, size);
        }

        private static IntPtr IntPtrSub(IntPtr a, IntPtr b)
        {
            return IntPtr.Size == 4 ? new IntPtr(a.ToInt32() - b.ToInt32()) : new IntPtr(a.ToInt64() - b.ToInt64());
        }

        private static bool IsInRange(IntPtr address, IntPtr start, IntPtr end)
        {
            if (IntPtr.Size == 4)
            {
                var val32 = (uint)address.ToInt32();
                return (uint)start.ToInt32() <= val32 && val32 <= (uint)end.ToInt32();
            }
            var val64 = (ulong)address.ToInt64();
            return (ulong)start.ToInt64() <= val64 && val64 <= (ulong)end.ToInt64();
        }
    }
}
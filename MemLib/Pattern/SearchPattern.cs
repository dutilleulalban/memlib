﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using MemLib.Pattern.SearchTypes;

namespace MemLib.Pattern
{
    public sealed class SearchPattern
    {
        private List<Func<RemoteProcess, IntPtr, IntPtr>> m_Funcs = new List<Func<RemoteProcess, IntPtr, IntPtr>>();
        private List<byte> m_Bytes = new List<byte>();
        private List<bool> m_Mask = new List<bool>();

        public byte[] Bytes => m_Bytes.ToArray();
        public bool[] Mask => m_Mask.ToArray();

        public int Length => m_Bytes.Count;

        public IntPtr ApplyFuncs(RemoteProcess process, IntPtr address)
        {
            foreach (var func in m_Funcs)
            {
                if (address == IntPtr.Zero)
                    break;
                address = func(process, address);
            }
            return address;
        }

        public void AddByte(byte value)
        {
            m_Bytes.Add(value);
            m_Mask.Add(false);
        }

        public void AddWildCard()
        {
            m_Bytes.Add(0);
            m_Mask.Add(true);
        }

        public void AddFunc(Func<RemoteProcess, IntPtr, IntPtr> func) => m_Funcs.Add(func);

        public IEnumerable<int> Search(byte[] data, int startIndex, bool matchFirstOnly)
        {
            var offset = startIndex;
            var bytes = Bytes;
            var mask = Mask;
            do
            {
                var index = BoyerMoore.IndexOf(data, bytes, mask, offset);
                if (index == -1) yield break;
                yield return index;
                if (matchFirstOnly) yield break;
                offset += index + Length;
            } while (offset < data.Length);
        }

        #region Overrides of Object

        public override string ToString()
        {
            var byteStr = string.Join(", ", m_Bytes.Select(b => $"{b:X2}"));
            var maskStr = string.Join("", m_Mask.Select(m => m ? "?" : "x"));
            return $"Bytes: [{byteStr}] Mask: {maskStr}";
        }

        #endregion
    }
}
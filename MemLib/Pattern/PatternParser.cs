﻿/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MemLib.Pattern
{
    public static class PatternParser
    {
        private static readonly HashSet<string> m_Tokens = new HashSet<string> {
            "Search", "TraceRelative",
            "Read8", "Read16", "Read32", "Read64",
            "Add", "Sub"
        };

        public static SearchPattern Parse(string patternString)
        {
            patternString = patternString.Trim();
            if (!patternString.StartsWith("Search")) patternString = $"Search {patternString}";
            var tokens = patternString.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var index = 0;
            var pattern = new SearchPattern();
            do
            {
                var text = tokens[index];
                if (text.Equals("Search"))
                {
                    index++;
                    var byteStrings = tokens[index].Length > 2 ?
                        SplitByteString(tokens[index]) :
                        tokens.Skip(index).TakeWhile(s => !IsToken(s));
                    foreach (var byteStr in byteStrings)
                    {
                        if (IsWildcard(byteStr))
                        {
                            pattern.AddWildCard();
                            continue;
                        }
                        if (!byte.TryParse(byteStr, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out var byteValue))
                            throw new ArgumentException("Could not parse search byte: " + byteStr, nameof(patternString));
                        pattern.AddByte(byteValue);
                    }
                    index += tokens[index].Length > 2 ? 1 : pattern.Length;
                    if (pattern.Length == 0)
                        throw new ArgumentException("Empty 'Search' token found.", nameof(patternString));
                }
                else if (text.Equals("Add"))
                {
                    index++;
                    if (tokens[index].StartsWith("#") && int.TryParse(tokens[index].Replace("#", ""), out var valInt) ||
                        int.TryParse(tokens[index], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out valInt))
                    {
                        pattern.AddFunc((process, ptr) =>
                        {
                            var val = valInt;
                            return ptr + val;
                        });
                    }
                    else throw new ArgumentException($"Could not parse 'Add' arg: {tokens[index]}", nameof(patternString));
                    index++;
                }
                else if (text.Equals("Sub"))
                {
                    index++;
                    if (tokens[index].StartsWith("#") && int.TryParse(tokens[index].Replace("#", ""), out var valInt) ||
                        int.TryParse(tokens[index], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out valInt))
                    {
                        pattern.AddFunc((process, ptr) =>
                        {
                            var val = valInt;
                            return ptr - val;
                        });
                    }
                    else throw new ArgumentException($"Could not parse 'Sub' arg: {tokens[index]}", nameof(patternString));
                    index++;
                }
                else if (text.Equals("Read8"))
                {
                    index++;
                    pattern.AddFunc((process, ptr) => process.TryRead<byte>(ptr, out var val) ? new IntPtr(val) : IntPtr.Zero);
                }
                else if (text.Equals("Read16"))
                {
                    index++;
                    pattern.AddFunc((process, ptr) => process.TryRead<short>(ptr, out var val) ? new IntPtr(val) : IntPtr.Zero);
                }
                else if (text.Equals("Read32"))
                {
                    index++;
                    pattern.AddFunc((process, ptr) => process.TryRead<int>(ptr, out var val) ? new IntPtr(val) : IntPtr.Zero);
                }
                else if (text.Equals("Read64"))
                {
                    index++;
                    pattern.AddFunc((process, ptr) => process.TryRead<long>(ptr, out var val) ? new IntPtr(val) : IntPtr.Zero);
                }
                else if (text.Equals("TraceRelative"))
                {
                    index++;
                    pattern.AddFunc((process, ptr) =>
                    {
                        if (process.Is64Bit)
                            return process.TryRead<int>(ptr, out var offset) ? ptr + 4 + offset : IntPtr.Zero;
                        return process.TryRead<int>(ptr, out var offset32) ? new IntPtr(offset32) : IntPtr.Zero;
                    });
                }
                else index++;
            } while (index < tokens.Length);

            return pattern;
        }

        private static bool IsToken(string text)
        {
            return m_Tokens.Contains(text);
        }

        private static bool IsWildcard(string text)
        {
            return text.Equals("??", StringComparison.Ordinal) ||
                   text.Equals("**", StringComparison.Ordinal) ||
                   text.Equals("?", StringComparison.Ordinal) ||
                   text.Equals("*", StringComparison.Ordinal);
        }

        private static IEnumerable<string> SplitByteString(string str)
        {
            var len = str.Length + (str.Length % 2 == 0 ? 0 : 1);
            for (var i = 0; i < len; i += 2)
                yield return str.Substring(i, i + 2 > str.Length ? 1 : 2);
        }
    }
}